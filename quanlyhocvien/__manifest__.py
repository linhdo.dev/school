# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'School Module',
    'version' : '1.1',
    'summary': 'School',
    'sequence': 15,
    'description': """
    school
    """,
    'category': '',
    'website': '',
    'images' : [],
    'depends' : [
        'contacts'
        ],
    'data': [
    "security/school_security.xml",
    "security/ir.model.access.csv",
    "data/data.xml",
    "data/cron_avg_point.xml",
    "data/mail_template.xml",
    "views/course_views.xml",
    "views/faculty_views.xml",
    "views/teacher_views.xml",
    "views/subject_views.xml",
    "views/point_views.xml",
    "views/condition_views.xml",
    "views/shift_views.xml",
    "views/student_views.xml",
    "views/registration_views.xml",
    "views/presence_views.xml",
    "views/outcome_views.xml",
    "views/result_views.xml",
    "views/fee_views.xml",
    "views/history_views.xml",
    "views/promotion_views.xml",
    "views/res_partner_views.xml",
    "views/res_user_views.xml",
    "wizards/student_status_views.xml",
    "reports/students_result_report.xml",
    "reports/students_result_report_templates.xml"
    
    
    ],
    'demo': [],
    'qweb': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'post_init_hook': '',
}