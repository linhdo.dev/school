from odoo import fields, models, api

class PaySchoolFee(models.Model):
    _name = "school.fee"
    _rec_name = "student_id"
    
    @api.model
    def domain_course(self):
        if self.student_id:
            rel_std = self.env["school.registration"].search([('student_id','=',self.student_id.id)])
            sb_arr =[]
            for course_sr in rel_std:
                sb_arr.append(course_sr.course_id.id)
            return [('id', 'in', sb_arr )]
        
    student_id = fields.Many2one("school.students", string="Student Name")    
    student_code_id = fields.Char(string="Students Code", related="student_id.student_code")
    course_id = fields.Many2one("school.courses", "Course Name", domain=domain_course)
    fee_course = fields.Integer("Tuition")
    pay_fee = fields.Integer("Pay Fee")
    owed_fees = fields.Integer("Owed Fees")

    
    @api.onchange("student_id")
    def onchage_student_id(self):
        if self.student_id:
            rel_std = self.env["school.registration"].search([('student_id','=',self.student_id.id)])
            sb_arr =[]
            for course_sr in rel_std:
                sb_arr.append(course_sr.course_id.id)
            return {'domain': {'course_id': [('id', 'in', sb_arr )]}}
    
    @api.onchange("course_id")
    def onchange_course_id(self):
        if self.course_id:
            regist = [(5,0)]
            fee_sum = 0
            for subject in self.course_id.subject_ids:
                regist.append((0,0,{
                        'subject_id' : subject.subject_id.id,
                        'tuition' : subject.subject_id.tuition,
                        'outcomes_id': self.id
                    }))
                fee_sum += subject.tuition
            self.fee_course = fee_sum
            self.owed_fees = self.fee_course
            
    @api.onchange("pay_fee") 
    def onchange_pay_fee(self):
        if self.pay_fee:
            self.owed_fees = self.fee_course - self.pay_fee
        
            
            
            
    
                        
             
            
            
        
    
    
    
    
    
    
    