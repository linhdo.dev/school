from odoo import fields, models, api

class LearnHistory(models.Model):
    _name = "school.history"
    _rec_name = "student_id"

    outcomes_id = fields.Many2one("school.results.detail", "Result Student")
    student_id = fields.Many2one("school.students", string="Student Name")    
    student_code_id = fields.Char(string="Students Code", related="student_id.student_code")
    course_id = fields.Many2one("school.courses", "Course Name")
    course_detail = fields.One2many("school.history.detail", "outcomes_id", "Detail Course")

    @api.onchange("student_id")
    def onchage_student_id(self):
        if self.student_id:
            rel_std = self.env["school.registration"].search([('student_id','=',self.student_id.id)])
            sb_arr =[]
            for course_sr in rel_std:
                sb_arr.append(course_sr.course_id.id)
            return {'domain': {'course_id': [('id', 'in', sb_arr )]}}
                     
             
class DetailHstory(models.Model): 
    _name = "school.history.detail"  
    
    outcomes_id = fields.Many2one("school.history", "Course Detail")
    
    
    
        
    
    
    
    
    
    
    