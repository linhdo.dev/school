from odoo import fields, models, api

class Faculty(models.Model):
    _name = "school.faculty"
    
    name = fields.Char("Faculty Name")
class User(models.Model):
    _inherit = "res.users"
    
    faculty_id = fields.Many2one("school.faculty", "Faculty Name") 
    

