from odoo import fields, models, api

class Subjects(models.Model):
    _name = "school.conditions"
    
    name = fields.Char("Subject Name")
    point = fields.Float("Point")
    apply_subjects = fields.One2many("school.subjects", "condition_id", "Apply Subject" )