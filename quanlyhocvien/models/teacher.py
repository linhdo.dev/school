from odoo import fields, models, api
from datetime import date, datetime


gender_list = [
    ('unknown','Unknown'),
    ('male','Male'),
    ('female','Female'),
    ('other','Other')
]
class Teachers(models.Model):
    _name = "school.teachers"
    
    name = fields.Char("Teacher Name")
    dob = fields.Date("Date Of Birth")
    gender = fields.Selection(gender_list,"Gender")
    faculty = fields.Many2one("school.faculty", "Faculty Name")
    list_students = fields.One2many("school.teachers.students", "teacher_ids", "List Student" )    
    
#     @api.onchange("faculty")
#     def onchange_course_id(self):
#         std = self.env["school.students"].search([('list_students','=',self.list_students.student_id.id)])
#         std_arr =[]
#         for student in std.std_arr:
#             std_arr.append(std_arr.student_id.id)
#         return {'domain': {'exam_subjects': [('id', 'in', std_arr )]}}
#         @api.onchange("course_id")
#         
#     def onchange_faculty(self):
#         if self.faculty:
#             regist = [(5,0)]
#             for students in self.faculty.student_id:
#                 regist.append((0,0,{
#                         'student_id' : subject.subject_id.id,
#                         'outcomes_id': self.id
#                     }))
#             self.subject_detail = regist   
        
class ListStudents(models.Model):
    _name = "school.teachers.students"
    
    teacher_ids = fields.Many2one("school.teachers", "Exam Subject")
    student_id = fields.Many2one("school.students", string="Student Name")    
    student_code_id = fields.Char(string="Students Code", related="student_id.student_code")


