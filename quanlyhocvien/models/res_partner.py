from odoo import api, fields, models

class Partner(models.Model):
    _inherit = 'res.partner'
    
    identity_card = fields.Char("Identity Card")
    relation = fields.Many2one("relation.students", "Relation Name")
    relation_student = fields.Many2one("school.students", "Relation Name")
    
    
    
class InheritRelation(models.Model):
    _name = "relation.students"
    
    name = fields.Char("Relation Name")
    