from odoo import fields, models, api

class Outcomes(models.Model):
    _name = "school.outcomes"
    _rec_name = "student_id"

    student_id = fields.Many2one("school.students", string="Student Name")    
    student_code_id = fields.Char(string="Students Code", related="student_id.student_code")
    exam_subjects = fields.Many2one("school.subjects", "Exam Subjects")
    course_id = fields.Many2one("school.courses", "Course Name")
    subject_detail = fields.One2many("school.detail.point", "outcomes_id", "Detail Point Subject")
    
    @api.onchange("student_id")
    def onchage_student_id(self):
        if self.student_id:
            rel_std = self.env["school.registration"].search([('student_id','=',self.student_id.id)])
            sb_arr =[]
            for course_sr in rel_std:
                for subject_sr in course_sr.course_id.subject_ids:
                    sb_arr.append(subject_sr.subject_id.id)
             
            return {'domain': {'exam_subjects': [('id', 'in', sb_arr )]}}
    
        
    @api.onchange("exam_subjects")
    def onchage_subject_id(self):
        if self.student_id and self.exam_subjects:
            self.course_id = False
            rel_std = self.env["school.registration"].search([('student_id','=',self.student_id.id)])
            crs_arr =[]
            for course_sr in rel_std:
                for subject_sr in course_sr.course_id.subject_ids:
                    if self.exam_subjects.id == subject_sr.subject_id.id:
                        crs_arr.append(course_sr.course_id.id)
                        break
             
            return {'domain': {'course_id': [('id', 'in', crs_arr )]}}
        

class DetailPoint(models.Model):
    _name = "school.detail.point"
    
    outcomes_id = fields.Many2one("school.outcomes", "Exam Subject")
    subject_id = fields.Many2one(related = "outcomes_id.exam_subjects",store=True)
    student_id = fields.Many2one(related = "outcomes_id.student_id",store=True)
    course_id = fields.Many2one(related = "outcomes_id.course_id",store=True)
    exam_date = fields.Date("Exam Date")
    point_id = fields.Many2one("school.points", "Points Type")
    point_st = fields.Float("Point 1st")
    point_nd = fields.Float("Point 2nd")
    
    
    