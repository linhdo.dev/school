from odoo import fields, models, api
from posix import remove

class Courses(models.Model):
    _name = "school.courses"
    
    name = fields.Char("Course Name")
    start_time = fields.Date("Start Time")
    finish_time = fields.Date("Finish Time")
    fees_student = fields.Integer("Course Fees")
    subject_ids = fields.One2many("school.courses.subject", "course_id", "Subjects Name")
    state = fields.Selection(string="Status", selection=[('draft', 'Draft'), ('confirm', 'Confirm'),('cancel','Cancel')], default="draft")

    def action_comfirm(self):
        self.state = 'confirm'
    
    def action_cancel(self):
        self.state = 'cancel'
              
    @api.model
    def create(self,values):
        res = super(Courses, self).create(values)
        fee_sum = 0
        for item in res.subject_ids:
            fee_sum += item.tuition
        res.fees_student = fee_sum
        return res

    def write(self,values):
        remove_ids = []
        fee_add = 0
        if 'subject_ids' in values:
            for item in values['subject_ids']:
                if item[0] == 2:
                    remove_ids += [item[1]]
                elif item[0] == 0:
                    fee_add += item[2]['tuition']
                elif item[0] == 1:
                    if 'tuition' in item[2]:
                        remove_ids += [item[1]]
                        fee_add += item[2]['tuition']
            fee_re = 0
            fee_sum = 0
            remove_obj_ids = self.env["school.courses.subject"].browse(remove_ids) 
            for line in remove_obj_ids:
                fee_re += line.tuition
            fee_sum = self.fees_student + fee_add - fee_re
            values.update({'fees_student': fee_sum})
        res = super(Courses, self).write(values)
        return res   
     
class CoursesSubject(models.Model):
    _name = "school.courses.subject"
    
    course_id = fields.Many2one("school.courses", "Course Name")
    subject_id = fields.Many2one("school.subjects", "Subject Name")
    tuition = fields.Integer("Tuition")
    start_date = fields.Date("Start Date")
    
    @api.onchange("subject_id")
    def onchange_subject(self):
        if self.subject_id:
            self.tuition = self.subject_id.tuition
