from odoo import fields, models, api

class Subjects(models.Model):
    _name = "school.subjects"
    
    name = fields.Char("Subject Name")
    time = fields.Float("Time Learn")
    tuition = fields.Integer("Tuition")
    condition_id = fields.Many2one("school.conditions", "Pass Condition")
    