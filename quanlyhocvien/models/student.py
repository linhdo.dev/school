from odoo import fields, models, api
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

gender_list = [
    ('unknown','Unknown'),
    ('male','Male'),
    ('female','Female'),
    ('other','Other')
]
    
status_list = [
    ('prepared_for_admission', 'Prepared for Admission'),
    ('enrolled','Enrolled'),
    ('thought_or_graduated','Thought or Graduated')
]    
class Students(models.Model):
    _name = "school.students"
    
    teacher_ids = fields.Many2one("school.teachers", "Faculty Name")
    name = fields.Char("Student Name")
    student_code = fields.Char("Students Code")
    student_phone = fields.Char("Student Phone Number")
    date_of_birth = fields.Date("Date Of Birth")
    native_country = fields.Many2one("res.country.state", "Native Country", domain="[('country_id','=',241)]")
    identity_card = fields.Char("Identity Card")
    gender = fields.Selection(gender_list,"Gender")
    registion_count = fields.Integer("Registration Count", compute='_compute_resgistion_course_count')
    avg_point = fields.Float("AVG Point", compute='_compute_avg_points')
    attendance_count = fields.Integer("Attendance Student", compute='_compute_attendance')
    presence_count = fields.Integer("Presence Student", compute='_compute_attendance')
    age = fields.Char("Age")
    contact_student = fields.One2many("res.partner", "relation_student", " Student Contacts")
    status_student = fields.Selection(status_list,"Status")
    active = fields.Boolean("Active", default=True)
    email_id = fields.Char("Email")
    faculty = fields.Many2one("school.faculty", "Faculty Name")
    @api.model
    def create(self,vals):
        vals.update({'student_code': self.env['ir.sequence'].next_by_code('field.student.code.seq')})
        age = False
        if 'date_of_birth' in vals:
            date_of_birth = datetime.strptime(vals['date_of_birth'], DEFAULT_SERVER_DATE_FORMAT)
            age = relativedelta(date.today(), date_of_birth).years
        vals.update({'age':age})
        res = super(Students, self).create(vals)
        return res
    
    def write(self, values):
        if 'date_of_birth' in values:
            date_of_birth = datetime.strptime(values['date_of_birth'], DEFAULT_SERVER_DATE_FORMAT)
            age = relativedelta(date.today(), date_of_birth).years
            values.update({'age':age})   
        
        if 'status_student' in values:
            if values['status_student'] == 'thought_or_graduated':
                values.update({'active':False})
            else:
                values.update({'active':True})
        rtn = super(Students, self).write(values)
                
        return rtn
       
    def _compute_resgistion_course_count(self):
        for student in self:
            student.registion_count = self.env["school.registration"].search_count([('student_id','=',student.id)])
            
    def _compute_avg_points(self):
        for student in self:
            points = self.env["school.detail.point"].search([('student_id','=',student.id)])
            
            point_sum = 0
            sum_percent = 0
            
            for point in points:
                point_sum += point.point_nd * point.point_id.percent if point.point_nd else point.point_st * point.point_id.percent
                sum_percent += point.point_id.percent 
            avg = point_sum/ sum_percent if sum_percent else 0.0
            student.avg_point = avg
            
    def _compute_attendance(self):
        for student in self:
            student.attendance_count = self.env["school.attendance"].search_count([('student_id','=',student.id)])
            student.presence_count = self.env["school.attendance"].search_count([('student_id','=',student.id), ('attendance_student', '=', True)])
            
            
    def action_open_point(self):
        self.ensure_one()
        action = self.env.ref('quanlyhocvien.points_list_action').read()[0]
        action['context'] = {
            'default_student_id': self.id,
        }
        action['domain'] = [('student_id', '=', self.id)]
        return action
    
    
    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}
        if self.env.user.has_group('quanlyhocvien.group_school_teacher'):
            args += [('faculty', '=', self.env.user.faculty_id.id)]
        return super(Students, self)._search(args, offset, limit, order, count=count, access_rights_uid=access_rights_uid)
    
    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        if self.env.user.has_group('quanlyhocvien.group_school_teacher'):
            domain += [('faculty', '=', self.env.user.faculty_id.id)]
        return super(Students, self).read_group(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby, lazy=lazy)
    
        
        