from odoo import fields, models, api

class Registration(models.Model):
    _name = "school.registration"
    _rec_name = "student_id"
    
    student_id = fields.Many2one("school.students", "Students Name")
    student_phone = fields.Char("Student Phone", store=True)
    course_id = fields.Many2one("school.courses", "Course Name")
    registration_date = fields.Date("Registration Date")
    registration_subject_ids = fields.One2many("school.registration.subject", "registration_id", "Subject Name")
    state = fields.Selection(string="Status", selection=[('draft', 'Draft'), ('confirm', 'Confirm'),('cancel','Cancel')], default="draft")
    
    def action_comfirm(self):
        self.state = 'confirm'
    
    def action_cancel(self):
        self.state = 'cancel'
    
    @api.onchange("course_id", "registration_date" )
    def onchange_course_id(self):
        if self.course_id:
            rel_prm = self.env["school.promotions"].search([('start_date','<=',self.registration_date), 
                                                            ('finish_date','>=',self.registration_date)])
            regist = [(5,0)]
            for subject in self.course_id.subject_ids:
                sum_promo = 0
                for promo in rel_prm:
                    if subject.subject_id.id in promo.app_subjects.ids:
                        sum_promo += promo.discount
                regist.append((0,0,{
                        'subject_id' : subject.subject_id.id,
                        'tuition' : subject.subject_id.tuition * (1.0 - sum_promo) if sum_promo < 1.0 else 0,
                        'registration_id': self.id
                    }))
            self.registration_subject_ids = regist
            
    @api.onchange("student_id")
    def phone_student(self):
        if self.student_id:
            self.student_phone = self.student_id.student_phone        
    @api.model        
    def create(self,values):
        res = super(Registration, self).create(values)
        if res.student_phone and res.student_phone != res.student_id.student_phone :
            res.student_id.student_phone = res.student_phone
        return res
    
    def write(self, values):
        rtn = super(Registration, self).write(values)
        for item in self:
            if item.student_phone and item.student_phone != item.student_id.student_phone :
                item.student_id.student_phone = item.student_phone   
        return rtn
                
   
    
class RegistrationSubject(models.Model):
    _name = "school.registration.subject"
    
    subject_id = fields.Many2one("school.subjects", "Subject Name")
    shift_id = fields.Many2one("school.shifts", "Shift")
    tuition = fields.Integer("Tuition")
    registration_id = fields.Many2one("school.registration", "Registration Name" )
    
    @api.onchange("subject_id")
    def onchange_subject(self):
        if self.subject_id:
            self.tuition = self.subject_id.tuition
            
    
            