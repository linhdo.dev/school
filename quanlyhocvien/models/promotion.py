from odoo import fields, models, api

class Attendance(models.Model):
    _name = "school.promotions"

    name = fields.Char("Promotion Name")
    start_date = fields.Date("Start Date")
    finish_date = fields.Date("Finish Date")
    discount = fields.Float("Discount")
    app_subjects = fields.Many2many("school.subjects", 'subject_promotion_rel', "subject_id", "promotion_id", "Apply Subjects")