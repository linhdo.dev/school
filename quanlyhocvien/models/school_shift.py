from odoo import fields, models, api

class Subjects(models.Model):
    _name = "school.shifts"
    
    name = fields.Char("School Shift Name")
    start_time = fields.Float("Start Time")
    finish_time = fields.Float("Finish Time")