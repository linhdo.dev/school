from odoo import fields, models, api

class Outcomes(models.Model):
    _name = "school.results"
    _rec_name = "student_id"

    student_id = fields.Many2one("school.students", string="Student Name")    
    student_code_id = fields.Char(string="Students Code", related="student_id.student_code")
    course_id = fields.Many2one("school.courses", "Course Name")
    subject_detail = fields.One2many("school.results.detail", "outcomes_id", "Detail Point")
    state = fields.Selection(string="Status", selection=[('send email', 'Send Email')])
    @api.onchange("student_id")
    def onchage_student_id(self):
        if self.student_id:
            rel_std = self.env["school.registration"].search([('student_id','=',self.student_id.id)])
            sb_arr =[]
            for course_sr in rel_std:
                sb_arr.append(course_sr.course_id.id)
            return {'domain': {'course_id': [('id', 'in', sb_arr )]}}
    
    @api.onchange("course_id")
    def onchange_course_id(self):
        if self.course_id:
            regist = [(5,0)]
            for subject in self.course_id.subject_ids:
                regist.append((0,0,{
                        'subject_id' : subject.subject_id.id,
                        'outcomes_id': self.id
                    }))
            self.subject_detail = regist
            
    def action_send_email(self):       
        template_id = self.env.ref('quanlyhocvien.result_email_template').id
        template = self.env['mail.template'].browse(template_id)
        template.send_mail(self.id, force_send=True)
class ResultPoint(models.Model):
    _name = "school.results.detail"
    
    outcomes_id = fields.Many2one("school.results", "Exam Subject")
    subject_id = fields.Many2one("school.subjects", "Subject Name")
    avg_point = fields.Float("AVG Point")
    subject_pass = fields.Boolean("Subject Pass")
    
#     
#     @api.model
#     def compute_point(self):
#         for rs in self:
#             points = self.env["school.detail.point"].search(['&',('student_id','=',rs.outcomes_id.student_id.id),'&',
#                                                             ('course_id','=',rs.outcomes_id.course_id.id),
#                                                             ('subject_id','=',rs.subject_id.id)])
#              
#             point_sum = 0
#             sum_percent = 0
#              
#             for point in points:
#                 point_sum += point.point_nd * point.point_id.percent if point.point_nd else point.point_st * point.point_id.percent
#                 sum_percent += point.point_id.percent 
#             avg = point_sum/ sum_percent if sum_percent else 0.0
#             rs.avg_point = avg
#             rs.subject_pass = avg >= rs.subject_id.condition_id.point 
#                     


    def caculator_point(self):
        for rs in self.env['school.results.detail'].search([]):
            
            points = self.env["school.detail.point"].search(['&',('student_id','=',rs.outcomes_id.student_id.id),'&',
                                                            ('course_id','=',rs.outcomes_id.course_id.id),
                                                            ('subject_id','=',rs.subject_id.id)])
            point_sum = 0
            sum_percent = 0
             
            for point in points:
                point_sum += point.point_nd * point.point_id.percent if point.point_nd else point.point_st * point.point_id.percent
                sum_percent += point.point_id.percent 
            avg = point_sum/ sum_percent if sum_percent else 0.0
            rs.write({'avg_point': avg, 'subject_pass': avg >= rs.subject_id.condition_id.point })
             
    
    
    
    
    