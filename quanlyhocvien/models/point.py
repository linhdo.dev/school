from odoo import fields, models, api

class Points(models.Model):
    _name = "school.points"
    
    name = fields.Char("Point Name")    
    percent = fields.Float("Percent")
    apply_subjects = fields.Many2many("school.subjects", "subject_point_rel", "subject_id", "point_id", "Apply Subjects")