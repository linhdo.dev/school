from odoo import fields, models, api

class Attendance(models.Model):
    _name = "school.attendance"
    _rec_name = "student_id"

    student_id = fields.Many2one("school.students", string="Student Name")    
    student_code_id = fields.Char(string="Students Code", related="student_id.student_code")
    learn_day = fields.Date(string="Learn Day")
    attendance_student = fields.Boolean("Student Attendance")