from odoo import models, fields, api

status_list = [
    ('prepared_for_admission', 'Prepared for Admission'),
    ('enrolled','Enrolled'),
    ('thought_or_graduated','Thought or Graduated')
] 

class StudentStatusWizard(models.TransientModel):
    _name = 'students.status.wizard'
    _description = "Students Status"

    student_status = fields.Selection(status_list,"Status")
    
    def action_apply(self):
        if self.student_status:
            student = self.env["school.students"].browse(self._context.get('active_ids'))
            for item in student:
                item.student_status = student.write({'status_student' : self.student_status})
               
